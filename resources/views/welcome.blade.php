@extends('layouts.structure')
@section('content')
<div class="container">
    <h1 class="text-center">Search recipe puppy 24/7 time ;)</h1>
    <div class="col-lg-12 col-xs-12">
        <label for="keyword">Let's search!</label>
        <input type="text" id="keyword" name="findKeyword" class="form-control">
    </div>
    <div class="col-lg-12 col-xs-12">
        <a href="javascript:void(0)" data-page="1" id="nextResults">Next</a>
        <ul id="results"></ul>
        
    </div>
</div>
@endsection