<html>
    <head>
        <title>Recipe Puppy Search</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        @yield('content')
        <script src='{{asset("js/webkit.js")}}'></script>
        <script src='{{asset("js/search.js")}}'></script>
        <script>search.init()</script>
    </body>
</html>