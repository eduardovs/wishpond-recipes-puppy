/**
 * Widget that help us with the search events
 */
const search = function(){
    function init(){
        setListeners()
    }

    function setListeners(){
        let keyword = document.getElementById("keyword")
        let next = document.getElementById("nextResults")

        keyword.addEventListener("keyup", function(){
            if(this.value.length >= 3){
                let formData = new FormData()
                formData.append("term", this.value)
                webkitFn.send(formData,'/search/recipes', resSearchKeyword)
            }
        })

        next.addEventListener("click", nextItem)
    }

    function resSearchKeyword(res){
        let results = document.getElementById("results")
        let nextResults = document.getElementById("nextResults")

        let elementHtml = ""
        for(let item  of res.results){
            let thumb = (item.thumbnail == "") ? 'https://via.placeholder.com/50': item.thumbnail
            elementHtml += `<li>
                <a href="${item.href}" target="_blank">
                    <img src="${thumb}" width="100"> ${item.title}
                    <span class="ingredients">Ingredients(${item.ingredients})</span>
                    <span>View more</span>
                </a>
            </li>`
        }
        nextResults.dataset.page = res.page
        results.innerHTML = (elementHtml == "") ? '<li>No hay registros</li>' : elementHtml;
    }

    function nextItem(e){
        let keyword = document.getElementById("keyword")
        let page = this.dataset.page

        let formData = new FormData()
        formData.append("term", keyword.value)
        formData.append("page", page)
        webkitFn.send(formData,'/search/recipes', resSearchKeyword)
    }

    return {
        init
    }
}()