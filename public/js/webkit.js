const webkitFn = function(){
    function send(data, uri, responseFn) {
        var crsf = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

        let req = new Request(uri, {
            method: "POST",
            mode: "same-origin",
            credentials: "same-origin",
            headers: {
                Accept: "application/json, text/plain, */*",
                "X-CSRF-Token": crsf,
                "X-Requested-With": "XMLHttpRequest"
            },
            body: data
        });
        fetch(req)
            .then(res => res.json())
            .then(res => responseFn(res, this));
    }

    return {
        send
    }
}()