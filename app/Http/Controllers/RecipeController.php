<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Clients\RecipesClient;

class RecipeController extends Controller
{
    public function find(Request $request)
    {
        if(!isset($request->term) && empty($request->term)){
            return 'Error query';
        }
        
        $search =  $this->queryBuilder($request);
        $limit = '';
        $indexPage = 1;
        if(isset($request->page) && !empty($request->page)){
            $limit = '&p='.$request->page;
            $indexPage = $request->page; 
            $indexPage++;
        }

        $client = new RecipesClient;
        
        $result = $client->checkout($search, $limit);
        $result->page = $indexPage;

        return json_encode($result);
    }

    private function queryBuilder($request)
    {
        $params = [];

        $params = [
            'q' =>  $request->term,
        ];

        return $params;
    }
}
