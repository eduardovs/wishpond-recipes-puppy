<?php
namespace App\Classes\Clients;

/**
 * Client receptor API from
 * Recipe Puppy 
 */
class RecipesClient
{
    private $url;

    public function __construct()
    {
        $this->url = 'http://www.recipepuppy.com/api/';
    }

    public function checkout($filters, $limit)
    {
        try{
            $response = $this->request($filters, $limit);
            if($response){
                return json_decode($response);
            }
        }catch (\Exception $e) {
            return false;
        }
    }

    private function request($request, $limit)
    {
        $data = http_build_query($request);
        $getUrl = $this->url."?".$data.$limit;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $getUrl);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}